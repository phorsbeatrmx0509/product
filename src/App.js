import "./App.css";
import Login from "./modules/login/Login";
import Register from "./modules/register/register";
import Home from "./modules/home/Home";
import Contact from "./modules/contact/Contact";
import Service from "./modules/service/Service";
import Product from "./modules/product/Product";
import UserPf from "./modules/userProfile/userPf";
import Layout from "./modules/layout/Layout";
import Missing from "./modules/missing/Missing";

import RequireAuth from "./modules/req/RequireAuth";
import { Routes, Route } from "react-router-dom";
import ProductDetail from "./modules/product/productDetail";

function App() {
  return (
    <Routes>
      <Route path="login" element={<Login />} />
      <Route path="register" element={<Register />} />

      <Route element={<RequireAuth />}>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<Home />} />
          <Route path="contact" element={<Contact />} />
          <Route path="service" element={<Service />} />
          <Route path="product" element={<Product />} />
          <Route path="product/:id" element={<ProductDetail />} />
          <Route path="userPf" element={<UserPf />} />
        </Route>
        <Route path="*" element={<Missing />} />
      </Route>
    </Routes>
  );
}

export default App;
