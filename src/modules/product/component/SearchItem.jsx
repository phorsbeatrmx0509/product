import React, { useState } from "react";
import { Button, Col, Form, Row } from "react-bootstrap";
import { searchProduct } from "../req";

const SearchItem = ({ setProducts }) => {
  const [searchProducts, setSearchProducts] = useState("");
  const onChangeSearch = () => {
    searchProduct({ q: searchProducts })
      .then((response) => {
        setProducts(response.data.products);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <>
      <Row>
        <Form inline>
          <Row>
            <Col xs="auto">
              <Form.Control
                type="text"
                placeholder="Search"
                className=""
                onChange={(e) => {
                  setSearchProducts(e.target.value);
                }}
              />
            </Col>
            <Col xs="auto">
              <Button onClick={onChangeSearch}>Search</Button>
            </Col>
          </Row>
        </Form>
      </Row>
    </>
  );
};

export default SearchItem;
