import { Button, Col, Row } from "react-bootstrap";

const CategoriesItem = ({ categories, active, setActive }) => {
  return (
    <div>
      <Row className="overflow-x-scroll">
        <Col className="d-flex ">
          <Button
            onClick={() => {
              setActive("ALL");
            }}
            variant={active === "ALL" ? "primary" : "secondary"}
          >
            ALL
          </Button>
          {categories.map((categories, index) => {
            return (
              <>
                <Button
                  key={index}
                  onClick={() => {
                    setActive(categories);
                  }}
                  variant={active === categories ? "primary" : "secondary"}
                >
                  {categories}
                </Button>
              </>
            );
          })}
        </Col>
      </Row>
    </div>
  );
};

export default CategoriesItem;
