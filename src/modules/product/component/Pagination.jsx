import React from "react";
import { Pagination } from "react-bootstrap";

const PaginationItem = ({ postPerPage, totalPosts, pagination }) => {
  const pageNumber = [];
  let active = 1;

  for (let i = 1; i <= Math.ceil(totalPosts / postPerPage); i++) {
    pageNumber.push(
      <Pagination.Item
        onClick={() => {
          pagination(i);
        }}
        active={i + 1 === active}
      >
        {i}
      </Pagination.Item>
    );
  }

  return (
    <>
      <Pagination className="justify-content-center p-2">
        <Pagination.First />
        <Pagination.Prev />
        <Pagination.Item></Pagination.Item>
        <Pagination.Ellipsis />
        {pageNumber}
        <Pagination.Ellipsis />
        <Pagination.Next />
        <Pagination.Last />
      </Pagination>
    </>
  );
};

export default PaginationItem;
