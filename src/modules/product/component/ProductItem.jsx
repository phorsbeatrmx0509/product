import React from "react";
import { Button, Card } from "react-bootstrap";

const ProductItem = ({ products, navigate }) => {
  return (
    <>
      <div className="row">
        {products.length === 0 && <div>Not Found</div>}

        {products.map((products, id) => {
          return (
            <div className="col-lg-3 col-md-6 col-sm-12 mb-3" key={id}>
              <Card style={{ width: "100%" }} key={id}>
                <Card.Img
                  key={id}
                  src={products.thumbnail}
                  alt={products.title}
                  style={{ width: "100%", height: "200px" }}
                />
                <Card.Body>
                  <Card.Title>{products.title}</Card.Title>
                  <div className="d-flex justify-content-between">
                    <p className="fw-bold">{products.brand}</p>
                    <p className="fw-bold text-danger">{products.price}$</p>
                  </div>
                  <Button
                    variant="primary"
                    onClick={() => {
                      navigate(`/product/${products.id}`);
                    }}
                  >
                    Details
                  </Button>
                </Card.Body>
              </Card>
            </div>
          );
        })}
      </div>
    </>
  );
};

export default ProductItem;
