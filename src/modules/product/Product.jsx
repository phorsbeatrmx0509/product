import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";
import { getProductCategories, getProducts, getListCategories } from "./req";
import ProductItem from "./component/ProductItem";
import PaginationItem from "./component/Pagination";
import CategoriesItem from "./component/CategoriesItem";
import SearchItem from "./component/SearchItem";

function Product() {
  const [products, setProducts] = useState([]);

  const [active, setActive] = useState("ALL");

  const [categories, setCategories] = useState([]);
  const navigate = useNavigate();
  const [currentPage, setCurrentPage] = useState(1);
  const [itemPerPage] = useState(12);

  // responseData
  useEffect(() => {
    getProducts(100).then((response) => {
      setProducts(response.data.products);
    });

    getListCategories().then((response) => {
      setCategories(response.data);
    });
  }, []);

  // CategoriesBtn

  useEffect(() => {
    active === "ALL"
      ? getProducts(100).then((response) => {
          setProducts(response.data.products);
        })
      : getProductCategories(100, active).then((response) => {
          setProducts(response.data.products);
        });
  }, [active]);

  // Get current
  const indexOfLastItem = currentPage * itemPerPage;
  const indexOfFirstItem = indexOfLastItem - itemPerPage;
  const currentItem = products.slice(indexOfFirstItem, indexOfLastItem);
  // pagination
  const pagination = (pageNumber) => setCurrentPage(pageNumber);

  return (
    <>
      <Container className="text-center p-5">
        <h1>Product Item</h1>
        <SearchItem setProducts={setProducts} />

        <CategoriesItem
          categories={categories}
          active={active}
          setActive={setActive}
        />

        <ProductItem products={currentItem} navigate={navigate} />

        <PaginationItem
          postPerPage={itemPerPage}
          totalPosts={products.length}
          pagination={pagination}
        />
      </Container>
    </>
  );
}

export default Product;
