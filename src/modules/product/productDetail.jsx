import React, { useEffect, useState } from "react";
import { getProductById } from "./req";
import { useParams } from "react-router-dom";
import { Col, Container, Image, Row } from "react-bootstrap";

function ProductDetail() {
  const [products, setProducts] = useState([]);
  const { id } = useParams();
  const [images, setImages] = useState([]);
  useEffect(() => {
    getProductById(id)
      .then((response) => {
        setProducts(response.data);
        setImages(response.data.images);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [id]);

  return (
    <>
      <Container>
        <Row>
          <Col className="text-center p-5">
            <h1>Product Details</h1>
          </Col>
        </Row>

        <Row>
          <Col className="d-flex justify-content-center">
            <Image src={products.thumbnail} className="w-100 h-100" />
          </Col>
          <Col className="">
            <h1>{products.title}</h1>
            <p>Rating {products.rating}</p>
            <hr />
            <div className="d-flex justify-content-between">
              <h3>{products.price} $</h3>
              <h6> In Stock {products.stock}</h6>
            </div>
            <h2>Overview</h2>
            <p>{products.description}</p>
          </Col>
        </Row>
        <Row>
          {images.map((images, index) => {
            return (
              <Col className="col-1">
                <img src={images} alt="" key={index} />;
              </Col>
            );
          })}
        </Row>
      </Container>
    </>
  );
}
export default ProductDetail;
