import axios from "axios";

const getProducts = (limit = 100) => {
  return axios.get("https://dummyjson.com/products", {
    params: { limit },
  });
};
const getProductById = (id) => {
  return axios.get(`https://dummyjson.com/products/${id}`);
};
const searchProduct = (params) => {
  return axios.get(`https://dummyjson.com/products/search`, {
    params: params,
  });
};
const getListCategories = (limit = 100) => {
  return axios.get(`https://dummyjson.com/products/categories`, {
    params: { limit },
  });
};
const getProductCategories = (limit = 10, path = "") => {
  return axios.get(`https://dummyjson.com/products/category/${path}`, {
    params: { limit },
  });
};

export {
  getProducts,
  getProductById,
  searchProduct,
  getListCategories,
  getProductCategories,
};
