import React, { useEffect, useState } from "react";
import axios from "axios";
import { Container, Nav, Navbar } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";

export default function Header() {
  const [userName, setUserName] = useState("");

  useEffect(() => {
    (async () => {
      const { data } = await axios.get("https://dummyjson.com/auth/me");
      setUserName(data.lastName);
    })();
  }, []);

  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="dark"
        variant="dark"
        className=" p-4"
      >
        <Container fluid>
          <LinkContainer to="/">
            <Navbar.Brand>React-Bootstrap</Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="me-auto">
              <LinkContainer to="/">
                <Nav.Link>Home</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/product">
                <Nav.Link>Product</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/service">
                <Nav.Link>Service</Nav.Link>
              </LinkContainer>
              <LinkContainer to="/contact">
                <Nav.Link>Contact</Nav.Link>
              </LinkContainer>
            </Nav>
            <Nav>
              <Navbar.Text>Login to</Navbar.Text>
              <LinkContainer to="/userPf">
                <Nav.Link>{userName}</Nav.Link>
              </LinkContainer>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </>
  );
}
