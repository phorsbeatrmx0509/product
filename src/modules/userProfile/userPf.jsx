import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";
import "./userPf.css";
import { useSpring, animated } from "@react-spring/web";

function Number({ n }) {
  const { number } = useSpring({
    from: { number: 0 },
    number: n,
    delay: 200,
    config: { mass: 1, tension: 20, friction: 10 },
  });
  return (
    <animated.div className="count h2">
      {number.to((n) => n.toFixed(0))}
    </animated.div>
  );
}

export default function UserPf() {
  const [user, setUser] = useState("");

  useEffect(() => {
    const getUser = () => {
      axios
        .get("https://dummyjson.com/auth/me")
        .then((response) => {
          setUser(response.data);
          console.log(response.data);
        })
        .catch((err) => {
          console.log(err);
        });
    };
    getUser();
  }, []);

  return (
    <Container>
      <section className="section about-section gray-bg" id="about">
        <div className="container">
          <div className="row align-items-center flex-row-reverse">
            <div className="col-lg-6">
              <div className="about-text go-to">
                <h3 className="dark-color">{`${user.firstName} ${user.lastName}`}</h3>
                <h6 className="theme-color lead">{user.company?.department}</h6>
                <div className="row about-list">
                  <div className="col-md-6">
                    <div className="media">
                      <label>Birthday</label>
                      <p>{user.birthDate}</p>
                    </div>
                    <div className="media">
                      <label>Age</label>
                      <p>{user.age} Yr</p>
                    </div>
                    <div className="media">
                      <label>Residence</label>
                      <p>Canada</p>
                    </div>
                    <div className="media">
                      <label>Address</label>
                      <p>{`${user.address?.address} ${user.address?.city} ${user.address?.state}`}</p>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="media">
                      <label>E-mail</label>
                      <p>{user.email}</p>
                    </div>
                    <div className="media">
                      <label>Phone</label>
                      <p>{user.phone}</p>
                    </div>
                    <div className="media">
                      <label>Skype</label>
                      <p>skype.0404</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-lg-6">
              <div className="about-avatar">
                <img src={user.image} title="" alt="" />
              </div>
            </div>
          </div>
          <div className="counter">
            <div className="row">
              <div className="col-6 col-lg-3">
                <div className="count-data text-center">
                  {<Number n={500} />}

                  <p className="m-0px font-w-600">Happy Clients</p>
                </div>
              </div>
              <div className="col-6 col-lg-3">
                <div className="count-data text-center">
                  {<Number n={150} />}

                  <p className="m-0px font-w-600">Project Completed</p>
                </div>
              </div>
              <div className="col-6 col-lg-3">
                <div className="count-data text-center">
                  {<Number n={200} />}
                  <p className="m-0px font-w-600">Photo Capture</p>
                </div>
              </div>
              <div className="col-6 col-lg-3">
                <div className="count-data text-center">
                  {<Number n={190} />}
                  <p className="m-0px font-w-600">Telephonic Talk</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Container>
  );
}
