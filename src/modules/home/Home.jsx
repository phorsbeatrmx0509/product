import axios from "axios";
import React, { useEffect, useState } from "react";
import { Container } from "react-bootstrap";

function Home() {
  const [nameUser, setNameUser] = useState("");
  useEffect(() => {
    (async () => {
      const { data } = await axios.get("https://dummyjson.com/auth/me");
      setNameUser(data.lastName);
    })();
  });
  return (
    <>
      <Container className="text-center">
        <h1>Hello Bro {nameUser}</h1>
      </Container>
    </>
  );
}

export default Home;
