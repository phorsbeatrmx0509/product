import axios from "axios";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBRow,
} from "mdb-react-ui-kit";
import React, { useEffect, useRef, useState } from "react";
import { Navigate } from "react-router-dom";

export default function Register() {
  const userRef = useRef();
  const [regUser, setRegUser] = useState({});
  const [regPw, setRegPw] = useState({});
  const [navigate, setNavigate] = useState(false);

  useEffect(() => {
    userRef.current.focus();
  }, [regPw, regUser]);
  const regUserSubmit = async (e) => {
    e.preventDefault();
    await axios.post("https://dummyjson.com/auth/login", {
      username: regUser,
      password: regPw,
    });
    setNavigate(true);
  };
  if (navigate) {
    return <Navigate to="/login" />;
  }
  return (
    <React.Fragment>
      <MDBContainer fluid>
        <MDBCard className="text-black m-5" style={{ borderRadius: "25px" }}>
          <MDBCardBody>
            <MDBRow>
              <MDBCol
                md="10"
                lg="6"
                className="order-2 order-lg-1 d-flex flex-column align-items-center"
              >
                <form onSubmit={regUserSubmit}>
                  <h1 classNAme="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">
                    Register
                  </h1>
                  <p>
                    dak ah tdea ban der <br /> User :kminchelle <br />
                    PW :0lelplR
                  </p>
                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="envelope me-3" size="lg" />
                    <MDBInput
                      htmlFor="username"
                      label="UserName"
                      id="form2"
                      type="text"
                      ref={userRef}
                      autoComplete="off"
                      onChange={(e) => setRegUser(e.target.value)}
                      required
                    />
                  </div>

                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="lock me-3" size="lg" />
                    <MDBInput
                      htmlFor="password"
                      label="Password"
                      id="form3"
                      type="password"
                      ref={userRef}
                      onChange={(e) => setRegPw(e.target.value)}
                    />
                  </div>
                  <MDBBtn className="mb-4" size="lg">
                    Register
                  </MDBBtn>
                </form>
              </MDBCol>

              <MDBCol
                md="10"
                lg="6"
                className="order-1 order-lg-2 d-flex align-items-center"
              >
                <MDBCardImage
                  src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
                  fluid
                />
              </MDBCol>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    </React.Fragment>
  );
}
