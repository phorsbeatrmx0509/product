const setAuth = (value) => localStorage.setItem("token", value);

const getAuth = () => localStorage.getItem("token");

const removeAuth = () => localStorage.removeItem("token");

export { setAuth, removeAuth, getAuth };
