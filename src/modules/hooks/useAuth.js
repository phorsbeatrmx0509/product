import { useContext } from "react";
import AuthContext from "../context/AuthProdvider";

const useAuth = () => {
  return useContext(AuthContext);
};
export default useAuth;
