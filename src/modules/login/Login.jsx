import React, { useEffect, useRef, useState } from "react";
import "mdb-react-ui-kit/dist/css/mdb.min.css";
import useAuth from "../hooks/useAuth";
import { useNavigate, useLocation } from "react-router-dom";
import {
  MDBBtn,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBCol,
  MDBContainer,
  MDBIcon,
  MDBInput,
  MDBRow,
} from "mdb-react-ui-kit";
import { login } from "../api/axios";
import { LinkContainer } from "react-router-bootstrap";
import axios from "axios";

function Login() {
  const { setAuth } = useAuth();
  const navigate = useNavigate();
  const location = useLocation();
  const from = location.state?.from?.pathnamne || "/";

  const userRef = useRef();
  const errRef = useRef();

  const [user, setUser] = useState("");
  const [pw, setPw] = useState("");
  const [errMsg, setErrMsg] = useState("");

  useEffect(() => {
    userRef.current.focus();
  }, []);

  useEffect(() => {
    setErrMsg("");
  }, [user, pw]);

  const handleSubmit = async (e) => {
    e.preventDefault();

    login(user, pw)
      .then((response) => {
        const accessToken = response?.data?.token;
        setAuth({ user, pw, accessToken });
        setErrMsg("");
        setUser("");
        setPw("");
        navigate(from, { replace: true });
        axios.defaults.headers.common[
          "Authorization"
        ] = `Bearer ${accessToken}`;
      })
      .catch((err) => {
        if (!err?.response) {
          setErrMsg("NO RESPONSE");
        } else if (err.response?.status === 400) {
          setErrMsg("Khos hz nos");
        } else {
          setErrMsg("Khos hz");
        }
        errRef.current.focus();
      });
  };

  return (
    <React.Fragment>
      <MDBContainer fluid>
        <MDBCard className="text-black m-5" style={{ borderRadius: "25px" }}>
          <MDBCardBody>
            <MDBRow>
              <MDBCol
                md="10"
                lg="6"
                className="order-2 order-lg-1 d-flex flex-column align-items-center"
              >
                <h6 ref={errRef} style={{ color: "red" }}>
                  {errMsg}
                </h6>

                <form onSubmit={handleSubmit}>
                  <h1 className="text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4">
                    Login
                  </h1>
                  <span>
                    User :kminchelle <br />
                    PW :0lelplR
                  </span>
                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="envelope me-3" size="lg" />
                    <MDBInput
                      htmlFor="username"
                      label="UserName"
                      id="form2"
                      type="text"
                      ref={userRef}
                      autoComplete="off"
                      onChange={(e) => setUser(e.target.value)}
                      value={user}
                      required
                    />
                  </div>

                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="lock me-3" size="lg" />
                    <MDBInput
                      htmlFor="password"
                      label="Password"
                      id="form3"
                      type="password"
                      ref={userRef}
                      onChange={(e) => setPw(e.target.value)}
                      value={pw}
                    />
                  </div>
                  <MDBBtn className="mb-4" size="lg">
                    Login
                  </MDBBtn>
                  <p>Ot ton mean acc men te ?</p>
                  <LinkContainer to="/register">
                    <MDBBtn className="mb-4" size="lg">
                      Register
                    </MDBBtn>
                  </LinkContainer>
                </form>
              </MDBCol>

              <MDBCol
                md="10"
                lg="6"
                className="order-1 order-lg-2 d-flex align-items-center"
              >
                <MDBCardImage
                  src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
                  fluid
                />
              </MDBCol>
            </MDBRow>
          </MDBCardBody>
        </MDBCard>
      </MDBContainer>
    </React.Fragment>
  );
}
export default Login;
