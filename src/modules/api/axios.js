import axios from "axios";
import { getAuth } from "../auth/component/authHelp";

const login = (user, pw) => {
  return axios.post("https://dummyjson.com/auth/login", {
    username: user,
    password: pw,
  });
};

const getUser = () => {
  return axios.get("https://dummyjson.com/auth/me", {
    headers: {
      Authorization: `Bearer ${getAuth()}`,
    },
  });
};
export { login, getUser };
